<?php

require __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Console\Application;

$application = new Application('App', '0.1-dev');
$command = new App\Command\AllowedLicensesEnebaCommand();
$application->add($command);
$application->run();