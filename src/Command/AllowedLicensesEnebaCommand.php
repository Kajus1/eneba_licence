<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AllowedLicensesEnebaCommand extends Command
{

    protected static $defaultName = 'app.license';
//    private $allowedLicenses;
//
//    /**
//     * @return mixed
//     */
//    public function getAllowedLicenses()
//    {
//        return $this->allowedLicenses;
//    }
//
//    /**
//     * @param mixed $allowedLicenses
//     */
//    public function setAllowedLicenses($allowedLicenses): void
//    {
//        $this->allowedLicenses = $allowedLicenses;
//    }
    protected function configure(): void
    {
        $this
            // configure an argument
            ->addArgument('licenses', InputArgument::IS_ARRAY )
            // ...
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $inputSteam = stream_get_contents(STDIN);
        $output->writeln("The follow input was received...");
        $json = json_decode($inputSteam, true);
        $index = 0;

        foreach ($json['dependencies'] as $item) {
            foreach ($item['license'] as $value) {
                if (!in_array($value, $input->getArgument('licenses'))) {
                    $index =1;
                }
            }
        }
        return $index;
//        if ($index == 1) {
//            return 1;
//        } else return 0;
    }
    //su shell scriptas ir i git
    // build
    // #!/bin/bash
    //https://www.sitepoint.com/boxing-apps-phars-quickly-easily-box/
}